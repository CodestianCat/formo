const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const UserService = require('../services/UserService');
const nodemailer = require('nodemailer');
const phoneToken = require('generate-sms-verification-code');
const inlineCss = require('nodemailer-juice');

const BCRYPT_SALT_ROUNDS = 12;
const userService = new UserService();

function response(res, message, status, success) {
    res.status(status).json({
        success,
        message
    });
}

const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
        user: 'formonotes@gmail.com',
        pass: 'Formo123'
    }
});

transporter.use('compile', inlineCss())

router.get('/verifymail/:email', (req, res) => {
    if (req.params.email) {

        const generatedToken = phoneToken(8, { type: 'string' });

        transporter.sendMail({
            from: '"Formo"',
            to: req.params.email,
            subject: 'Formo Notes Registration',
            html: `
            <div style="box-sizing: border-box; margin:0; padding: 20px; width: 300px; background: #FFA200">
            <div style="box-sizing: border-box; margin:0; padding: 0; font-size: 22px; letter-spacing: 8px; color: white; font-weight: bold;">FORMO</div>
            <h1 style="box-sizing: border-box; margin:0; padding: 0; color: white">This is the verification code to register</h1>
            <br><br><br><br><br>
            <h3 style="text-align: center; box-sizing: border-box; margin:0; padding: 0; color: #FFA200; font-size: 18px; background-color: white; padding: 10px;">${generatedToken}</h3>
            </div>          
            `
        });
        response(res, generatedToken, 200, true);
    }
    else {
        response(res, 'please add in email', 400, false);
    }
});

router.get('/forgotpassword/:email', (req, res) => {
    userService.forgotPassword((req.params.email).toLowerCase())
        .then((data) => {
            transporter.sendMail({
                from: '"Formo"',
                to: req.params.email,
                subject: 'Formo Notes Forgot Password',
                html: `
                <div style="box-sizing: border-box; margin:0; padding: 20px; width: 300px; background: #FFA200">
                <div style="box-sizing: border-box; margin:0; padding: 0; font-size: 22px; letter-spacing: 8px; color: white; font-weight: bold;">FORMO</div>
                <h1 style="box-sizing: border-box; margin:0; padding: 0; color: white">You have requested to reset your password</h1>
                <br><br><br><br><br>
                <h3 style="text-align: center; box-sizing: border-box; margin:0; padding: 0; color: #FFA200; font-size: 18px; background-color: white; padding: 10px;">${req.protocol}://${req.get('host')}/reset/${data}</h3>
                </div> 
                `
            });
            response(res, data, 200, true)
        })
        .catch((err) => { response(res, err, 400, false) });
});

router.get('/checkfp/:token', (req, res) => {
    const { token } = req.params;
    userService.getUserFromForgetPassword(token)
        .then((data) => { response(res, data, 200, true) })
        .catch((err) => { response(res, err, 400, false) })
});

router.get('/resetpassword/:token/:password', (req,res) => {
    const { token, password } = req.params;
    bcrypt.hash(password, BCRYPT_SALT_ROUNDS,
        (err, newpassword) => {
            userService.resetPassword(token, newpassword)
                .then((data) => { response(res, data, 200, true) })
                .catch((err) => { response(res, err, 400, false) });
        });
});

// REGISTERS USER ACCOUNT
router.post('/register', (req, res) => {

    const { email, username, password } = req.body;

    if (email && username && password) {
        bcrypt.hash(password, BCRYPT_SALT_ROUNDS,
            (err, newpassword) => {
                userService.registerUser(email, username, newpassword)
                    .then((data) => { response(res, data, 200, true) })
                    .catch((err) => { response(res, err, 400, false) });
            });
    }
    else { response(res, 'please provide parameters $email, $username, $password', 400, false) }

});

// LOGS IN USER ACCOUNT, PROVIDING PROFILE AND ACCESSTOKEN
router.post('/login', (req, res) => {

    const { email, password } = req.body;

    if (email && password) {
        userService.loginUser(email, password)
            .then((data) => { response(res, data, 200, true) })
            .catch((err) => { response(res, err, 400, false) });
    }
    else { response(res, 'please provide email and password', 400, false) }

});

// LOGS OUT THE USER, REMOVING ACCESSTOKEN
router.get('/logout', (req, res) => {
    const accessToken = req.header('Authorization');
    if (accessToken) {
        userService.logout(accessToken)
            .then((data) => { response(res, data, 200, true) })
            .catch((err) => { response(res, err, 400, false) })
    }
    else { response(res, 'please provide header $Authorization', 401, false) }
});

// VERIFIES IF USER IS LOGGED IN
router.get('/verify/', (req, res) => {
    const accessToken = req.header('Authorization');
    if (accessToken) {
        userService.authUser(accessToken)
            .then((data) => { response(res, data, 200, true) })
            .catch((err) => { response(res, err, 400, false) });
    }
    else { response(res, 'please provide header $Authorization', 401, false) }
});

module.exports = router;