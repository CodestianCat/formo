const express = require('express');
const router = express.Router();
const UserService = require('../services/UserService');
const NoteService = require('../services/NoteService');
const multer = require('multer');

const userService = new UserService();
const noteService = new NoteService();

function response(res, message, status, success) {
    res.status(status).json({
        success,
        message
    });
}

router.use('/', (req, res, next) => {
    const accessToken = req.header('Authorization');
    if (accessToken) {

        userService.authUser(accessToken)
            .then((data) => {
                res.locals.token = data.accessToken;
                next();
            })
            .catch((err) => { response(res, err, 401, false) });

    }
    else { response(res, 'please provide header $Authorization', 401, false) }
});

router.post('/add', (req, res) => {
    const { title, body } = req.body;
    const date = new Date();
    const timestamp = date.toLocaleString();

    if (title && body) {
        noteService.addNote(res.locals.token, { title, body, date: timestamp })
            .then((data) => {
                response(res, data, 200, true);
            })
            .catch((err) => {
                response(res, err, 400, false);
            });
    }
    else { response(res, 'please provide note title and body', 400, false) }

});


router.delete('/delete/:id', (req, res) => {
    noteService.deleteNote(res.locals.token, req.params.id)
        .then((data) => {
            response(res, data, 200, true);
        })
        .catch((err) => {
            response(res, err, 400, false);
        });
});

router.put('/update/:id', (req, res) => {
    const { title, body } = req.body;
    const date = new Date();
    const timestamp = date.toLocaleString();

    if (title && body) {
        noteService.updateNote(res.locals.token, req.params.id, {title, body, date: timestamp})
            .then((data) => {
                response(res, data, 200, true);
            })
            .catch((err) => {
                response(res, err, 400, false);
            });
    }
    else { response(res, 'please provide note title and body', 400, false) }
});

module.exports = router;