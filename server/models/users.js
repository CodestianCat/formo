const mongoose = require('mongoose');
const notesSchema = require('./notes');

const usersSchema = mongoose.Schema({
    email: { type: String, unique: true, required: true},
    username: { type: String, required: true },
    password: { type: String, required: true},
    notes: [notesSchema],
    accessToken: { type: String },
    forgetPassToken: { type: String }
});

const User = mongoose.model('User', usersSchema);
module.exports = User;