const User = require('../models/users');
const bcrypt = require('bcryptjs');
const phoneToken = require('generate-sms-verification-code');

const BCRYPT_SALT_ROUNDS = 12;

class UserService {

    async authUser(accessToken) {
        try {
            let result = await User.findOne({ accessToken });
            if (result) { return Promise.resolve(result) }
            else { return Promise.reject('user is not logged in') }
        }
        catch (err) { return Promise.reject('unable to verify user token') };
    }

    async forgotPassword(email) {
        try {
            let result = await User.findOne({ email })
            if (result) {
                const generatedToken = phoneToken(20, { type: 'string' });
                result.forgetPassToken = generatedToken;

                try {
                    await result.save();
                    return Promise.resolve(generatedToken);
                }
                catch (err) { return Promise.reject('unable to execute reset password') }
            }
            else { return Promise.reject('user does not exist') }
        }
        catch (err) { return Promise.reject('user does not exist') }
    }

    async resetPassword(forgetPassToken, password) {
        try {
            let result = await User.findOne({forgetPassToken});
            if(result) {
                result.password = password;
                result.forgetPassToken = '';
                try {
                    await result.save();
                    return Promise.resolve('successfully reset password');
                }
                catch(err) {return Promise.reject('unable to reset password')}
            }
            else {return Promise.reject('unable to find user')}
        }
        catch (err) { return Promise.reject('unable to find user') }
    }

    async registerUser(email, username, password) {
        const user = new User({ email, username, password, notes: [], accessToken: '', forgetPassToken: '' });
        try {
            let result = await user.save();
            return Promise.resolve(result);
        }
        catch (err) { return Promise.reject('user has been registered') };
    }

    async loginUser(email, password) {
        try {
            let result = await User.findOne({ email });
            if (result) {
                try {
                    let test = await bcrypt.compare(password, result.password);
                    if (test) {
                        try {
                            let result2 = await bcrypt.hash(email, BCRYPT_SALT_ROUNDS);
                            result.accessToken = result2;
                            result.forgetPassToken = '';
                            try {
                                let result3 = await result.save();
                                return Promise.resolve(result3);
                            }
                            catch (err) { return Promise.reject('unable to login user') };
                        }
                        catch (err) { return Promise.reject('unable to login user') };
                    }
                    else { return Promise.reject('wrong password') };
                }
                catch (err) { return Promise.reject('wrong password') };
            }
            else { return Promise.reject('account does not exist') };
        }
        catch (err) { return Promise.reject('unable to login user') };
    }

    async logout(accessToken) {
        try {
            let result = await this.authUser(accessToken);
            result.accessToken = '';

            try {
                result1 = await result.save();
                return Promise.resolve('user successfully logged out');
            }
            catch (err) { return Promise.reject('unable to logout user') };

        }
        catch (err) { return Promise.reject(err) }
    }

    async getUserFromForgetPassword(forgetPassToken) {
        try {
            let result = await User.findOne({forgetPassToken});
            if(result) {
                return Promise.resolve('user found');
            }
            else {return Promise.reject('unable to find user')}
        }
        catch (err) { return Promise.reject('unable to find user') }
    }


}

module.exports = UserService;