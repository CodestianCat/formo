const User = require('../models/users');
const Note = require('../models/notes');

class NoteService {

    async getAllNotes(accessToken) {
        try {
            let result = await User.findOne({ accessToken });
            if (result) {
                return Promise.resolve(result.notes);
            }
            else {
                return Promise.reject('unable to verify user token');
            }
        }
        catch (err) { return Promise.reject('unable to verify user token') };
    }

    async addNote(accessToken, note) {
        try {
            let result = await User.findOne({ accessToken });
            if (result) {
                result.notes.push(note);
                try {
                    await result.save();
                    return Promise.resolve(result.notes);
                }
                catch (err) { return Promise.reject('unable to add note') };
            }
            else { return Promise.reject('unable to verify user token') }
        }
        catch (err) { return Promise.reject('unable to verify user token') };
    }

    async deleteNote(accessToken, id) {
        try {
            let result = await User.findOne({ accessToken });
            if (result) {
                result.notes.pull(id);
                try {
                    await result.save();
                    return Promise.resolve(result.notes);
                }
                catch (err) { return Promise.reject('unable to delete note') };
            }
            else { return Promise.reject('unable to verify user token') }
        }
        catch (err) { return Promise.reject('unable to verify user token') };
    }

    async updateNote(accessToken, id, note) {
        try {
            let result = await User.findOne({ accessToken });
            if (result) {
                const newnotes = result.notes.map((one) => {
                    return (one._id.toString() === id) ? {
                        _id: one._id,
                        title: note.title,
                        body: note.body,
                        date: note.date
                    } : one;
                });

                result.notes = newnotes;

                try {
                    await result.save();
                    return Promise.resolve(result.notes);
                }
                catch(err) {return Promise.reject('unable to update note')}
            }
            else { return Promise.reject('unable to verify user token') }
        }
        catch (err) { return Promise.reject('unable to verify user token') };
    }
}

module.exports = NoteService;