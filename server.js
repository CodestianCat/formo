// Get dependencies
const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const multer = require('multer');

const UserController = require('./server/controllers/UserController');
const NoteController = require('./server/controllers/NoteController');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

app.use(express.static(path.join(__dirname, 'dist')));

app.use('/api/users', UserController);
app.use('/api/notes', NoteController);

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
});
const port = process.env.PORT || '3000';

app.set('port', port);

const server = http.createServer(app);

mongoose.connect('mongodb://admin:admin12@ds253104.mlab.com:53104/formo', { useNewUrlParser: true, useCreateIndex: true }, ((err, database) => {
    if (err) return console.log(err);
    server.listen(port, () => console.log(`API running on localhost:${port}`));
}));