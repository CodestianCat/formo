import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgScrollbarModule } from 'ngx-scrollbar';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';

import {MatIconModule} from '@angular/material/icon';
import {HttpClientModule} from '@angular/common/http';

import { UserService } from '../services/user.service';
import { NoteService } from '../services/note.service';
import { ForgetpasswordComponent } from './forgetpassword/forgetpassword.component';

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    ForgetpasswordComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgScrollbarModule,
    MatIconModule,
    HttpClientModule
  ],
  providers: [UserService, NoteService],
  bootstrap: [AppComponent]
})
export class AppModule { }
