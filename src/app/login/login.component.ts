import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  registerEmail: string = '';
  registerUsername: string = '';
  registerPassword: string = '';
  registerConfirmPassword: string = '';

  loginEmail: string = '';
  loginPassword: string = '';

  loginTextRegister: string = '';
  loginTextLogin: string = '';
  loginTextVerify: string = '';
  loginTextForgot: string = '';

  loginButtonText: string = 'LOGIN';
  RegisterButtonText: string = 'REGISTER';

  verifyScreen: boolean = true;
  verifyCode: string = '';
  userVerifyCode: string = '';

  forgotScreen: boolean = true;
  forgotEmail: string = '';

  constructor(private router: Router, private userService: UserService) { }

  ngOnInit() {
  }

  onChangeRegisterEmail(event: any) { this.registerEmail = event.target.value }

  onChangeRegisterUsername(event: any) { this.registerUsername = event.target.value }

  onChangeRegisterPassword(event: any) { this.registerPassword = event.target.value }

  onChangeRegisterConfirmPassword(event: any) { this.registerConfirmPassword = event.target.value }

  onChangeLoginEmail(event: any) { this.loginEmail = event.target.value }

  onChangeLoginPassword(event: any) { this.loginPassword = event.target.value }

  onChangeVerifyCode(event: any) { this.userVerifyCode = event.target.value }

  onChangeForgotEmail(event: any) { this.forgotEmail = event.target.value }

  forgot() {
    this.forgotScreen = false;
  }

  verifyEmail() {
    const { registerEmail, registerUsername, registerPassword, registerConfirmPassword } = this;

    if (registerEmail && registerUsername && registerPassword && registerConfirmPassword) {
      if (registerPassword === registerConfirmPassword) {
        this.userService.verifyEmail(registerEmail).subscribe(
          (data: any) => {
            this.verifyScreen = false;
            this.verifyCode = data.message;
          }
        );
      }
      else { this.loginTextRegister = 'passwords do not match'.toUpperCase() }
    }
    else { this.loginTextRegister = 'please enter all credentials'.toUpperCase() }
  }

  forgotPassword() {
    let { forgotEmail } = this;

    if (forgotEmail) {
      this.userService.forgotPassword(forgotEmail).subscribe(
        (data: any) => {
          this.loginTextForgot = '';
          console.log(data);
        },
        (err: any) => {
          this.loginTextForgot = (err.error.message).toUpperCase();
        }
      );
    }
    else { this.loginTextForgot = 'please enter your email'.toUpperCase() }

  }

  register() {
    let { registerEmail, registerUsername, registerPassword } = this;

    this.RegisterButtonText = 'REGISTERING...';
    if (this.verifyCode === this.userVerifyCode) {
      this.loginTextVerify = '';
      this.userService.register({ email: registerEmail, username: registerUsername, password: registerPassword })
        .subscribe(
          (data: any) => {
            this.RegisterButtonText = 'REGISTER';


            this.userService.login({ email: registerEmail, password: registerPassword })
              .subscribe(
                (data: any) => {
                  sessionStorage.setItem("token", data.message.accessToken);
                  this.router.navigate(['/dashboard']);
                },
                (err: any) => {
                  this.loginTextVerify = 'an error occurred. please refresh the page'.toUpperCase();
                }
              );



          },
          (err: any) => {
            this.RegisterButtonText = 'REGISTER';
            this.loginTextVerify = (err.error.message).toUpperCase();
          }
        );
    }
    else {
      this.loginTextVerify = 'CODE DOES NOT MATCH';
      this.RegisterButtonText = 'REGISTER';
    }
  }

  login() {
    let { loginEmail, loginPassword } = this;

    this.loginButtonText = 'LOGGING IN...';
    this.loginTextLogin = '';

    this.userService.login({ email: loginEmail, password: loginPassword })
      .subscribe(
        (data: any) => {
          this.loginButtonText = 'LOGIN';
          sessionStorage.setItem("token", data.message.accessToken);
          this.router.navigate(['/dashboard']);
        },
        (err: any) => {
          this.loginButtonText = 'LOGIN';
          this.loginTextLogin = (err.error.message).toUpperCase();
        }
      );
  }

}
