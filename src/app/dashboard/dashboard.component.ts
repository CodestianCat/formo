import { Component, OnInit } from '@angular/core';
import axios from 'axios';
import { Router } from '@angular/router';

import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';
import _ from 'lodash';

import { UserService } from '../../services/user.service';
import { NoteService } from '../../services/note.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public account: any = {};

  public modal: any = {};

  public isProfileModalOpen = false;

  constructor(
    private router: Router,
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer,
    private userService: UserService,
    private noteService: NoteService) {
    iconRegistry.addSvgIcon(
      'close',
      sanitizer.bypassSecurityTrustResourceUrl('../../assets/icons/close.svg'));

    iconRegistry.addSvgIcon(
      'image',
      sanitizer.bypassSecurityTrustResourceUrl('../../assets/icons/image.svg'));

    iconRegistry.addSvgIcon(
      'delete',
      sanitizer.bypassSecurityTrustResourceUrl('../../assets/icons/delete.svg'));

    iconRegistry.addSvgIcon(
      'note',
      sanitizer.bypassSecurityTrustResourceUrl('../../assets/icons/note.svg'));

    iconRegistry.addSvgIcon(
      'logout',
      sanitizer.bypassSecurityTrustResourceUrl('../../assets/icons/logout.svg'));
  }

  onChangeTitle(title) {
    this.modal.title = title;
  }

  onChangeBody(body) {
    this.modal.body = body;
  }

  ngOnInit() {
    this.userService.verifyUser(sessionStorage.getItem("token")).subscribe(
      (data: any) => {
        this.account = data.message;
        this.closeAnimation = this.closeAnimation.bind(this);
        this.modalClose();
      },
      (err) => { this.router.navigate(['/login']) }
    );
  }

  openProfileModal(event) {

  }

  logout() {
    this.userService.logout(sessionStorage.getItem("token")).subscribe(
      (data: any) => {
        sessionStorage.setItem("token", "");
        this.router.navigate(['/login']);
      },
      (err: any) => { this.router.navigate(['/login']) }
    );
  }

  deleteNote() {
    const { id } = this.modal;
    const modal = document.getElementById('modal');
    const box = document.getElementById('box');
    if (id) {
      this.noteService.deleteNote(sessionStorage.getItem("token"), id).subscribe(
        (data: any) => {
          this.account.notes = data.message;
          modal.classList.replace("modal-opened", "modal-closed");
          setTimeout(() => {
            modal.classList.remove("demo");
            box.classList.remove("modalBoxWidthAfter");
          }, 0);
        },
        (err) => { this.router.navigate(['/login']) }
      )
    }
    else {
      modal.classList.replace("modal-opened", "modal-closed");
      setTimeout(() => {
        modal.classList.remove("demo");
        box.classList.remove("modalBoxWidthAfter");
      }, 0);
    }
  }

  openModal(note: any) {
    const modal = document.getElementById('modal');
    const box = document.getElementById('box');

    modal.classList.replace("modal-closed", "modal-opened");
    setTimeout(() => {
      modal.classList.add("demo");
      box.classList.add("modalBoxWidthAfter");
    }, 0);

    this.modal.title = note.title;
    this.modal.body = note.body;
    this.modal.id = note._id;
    this.modal.date = note.date;
  }

  openNewModal() {
    const date = new Date();
    console.log(date.toLocaleString());



    const modal = document.getElementById('modal');
    const box = document.getElementById('box');

    modal.classList.replace("modal-closed", "modal-opened");
    setTimeout(() => {
      modal.classList.add("demo");
      box.classList.add("modalBoxWidthAfter");
    }, 0);

    this.modal.title = '';
    this.modal.body = '';
    this.modal.id = '';
    this.modal.date = '';

  }

  modalClose() {
    const modal = document.getElementById('modal');
    const { closeAnimation } = this;
    window.onclick = function (event) {
      if (event.target == modal) {
        closeAnimation();
      }
    }
  }

  closeAnimation() {
    const modal = document.getElementById('modal');
    const box = document.getElementById('box');
    const { title, body, id } = this.modal;

    if (id) {
      this.noteService.updateNote(sessionStorage.getItem("token"), id, { title: this.modal.title, body: this.modal.body })
        .subscribe(
          (data: any) => {
            this.account.notes = data.message;
            modal.classList.replace("modal-opened", "modal-closed");
            setTimeout(() => {
              this.modal = {};
              modal.classList.remove("demo");
              box.classList.remove("modalBoxWidthAfter");
            }, 0);
          },
          (err) => { this.router.navigate(['/login']) }
        )
    }
    else {
      if (title && body) {
        this.noteService.addNote(sessionStorage.getItem("token"), { title, body }).subscribe(
          (data: any) => {
            this.account.notes = data.message;
            modal.classList.replace("modal-opened", "modal-closed");
            setTimeout(() => {
              this.modal = {};
              modal.classList.remove("demo");
              box.classList.remove("modalBoxWidthAfter");
            }, 0);
          },
          (err) => { this.router.navigate(['/login']) }
        );
      }
      else {
        modal.classList.replace("modal-opened", "modal-closed");
        setTimeout(() => {
          this.modal = {};
          modal.classList.remove("demo");
          box.classList.remove("modalBoxWidthAfter");
        }, 0);
      }

    }
  }
}
