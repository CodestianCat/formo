import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.component.html',
  styleUrls: ['./forgetpassword.component.scss']
})
export class ForgetpasswordComponent implements OnInit {

  constructor(private router: Router, private route: ActivatedRoute, private userService: UserService) { }

  changePassword: string = '';
  changeRetypePassword: string = '';
  resetButtonText = 'RESET PASSWORD';
  resetText = '';

  onChangePassword(event: any) { this.changePassword = event.target.value }

  onChangeRetypePassword(event: any) { this.changeRetypePassword = event.target.value }

  ngOnInit() {
    let token = this.route.snapshot.params.token;

    this.userService.getUserFromFP(token).subscribe(
      (data) => { console.log(data) },
      (err) => { this.router.navigate(['/']) }
    );
  }

  reset() {
    if (this.changePassword && this.changeRetypePassword) {
      if (this.changePassword === this.changeRetypePassword) {
        this.resetButtonText = 'RESETTING...'
        this.resetText = '';

        this.userService.resetPassword(this.route.snapshot.params.token, this.changePassword).subscribe(
          (data) => {
            this.resetButtonText = 'SUCCESSFUL';
            setTimeout(() => {
              this.router.navigate(['/login'])
            }, 500);
          },
          (err) => { this.resetText = 'UNABLE TO RESET PASSWORD'; }
          // (err) => { this.resetText = 'UNABLE TO RESET PASSWORD' }
        );
      }
      else { this.resetText = 'PASSWORDS DO NOT MATCH' }
    }
    else { this.resetText = 'PLEASE FILL IN ALL PARAMETERS' }
  }
}
