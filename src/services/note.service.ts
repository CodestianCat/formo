import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptionsToken = (accessToken) => {
    return new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': accessToken
    })
};

@Injectable()
export class NoteService {

    constructor(private http: HttpClient) { }

    public deleteNote(accessToken, id) {
        return this.http.delete(`/api/notes/delete/${id}`, { headers: httpOptionsToken(accessToken) });
    }

    public addNote(accessToken, note) {
        return this.http.post(`/api/notes/add`, note, { headers: httpOptionsToken(accessToken) });
    }

    public updateNote(accessToken, id, note) {
        return this.http.put(`/api/notes/update/${id}`, note, { headers: httpOptionsToken(accessToken) });
    }
}