import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};

const httpOptionsToken = (accessToken) => {
    return new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': accessToken
    })
};

@Injectable()
export class UserService {

    constructor(private http: HttpClient) { }

    public register(user) { return this.http.post('/api/users/register', user, httpOptions) }

    public login(user) { return this.http.post('/api/users/login', user, httpOptions) }

    public logout(accessToken) { return this.http.get('/api/users/logout', { headers: httpOptionsToken(accessToken) }) }

    public verifyUser(accessToken) { return this.http.get('/api/users/verify', { headers: httpOptionsToken(accessToken) }) }

    public verifyEmail(email) { return this.http.get(`/api/users/verifymail/${email}`) }

    public forgotPassword(email) { return this.http.get(`/api/users/forgotpassword/${email}`) }

    public getUserFromFP(token) { return this.http.get(`/api/users/checkfp/${token}`)}

    public resetPassword(token, password) { return this.http.get(`/api/users/resetpassword/${token}/${password}`)}
}

